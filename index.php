<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    $animal = new Animal("Shaun");
    echo "Name = ".$animal->name ."<br>";
    echo "Legs = ".$animal->legs ."<br>";
    echo "Cold Blooded = ".$animal->coldblooded ."<br>";
    
    echo "<br>";
    $frog = new Frog("Kuduk");
    echo "Name = ".$frog->name ."<br>";
    echo "Legs = ".$animal->legs ."<br>";
    echo "Cold Blooded = ".$animal->coldblooded ."<br>";
    echo $frog->Jump();

    echo "<br><br>";
    $ape = new KeraSakti("Kera Sakti");
    echo "Name = ".$ape->name ."<br>";
    echo "Legs = ".$ape->legs ."<br>";
    echo "Cold Blooded = ".$animal->coldblooded ."<br>";
    echo $ape->Yell();

?>